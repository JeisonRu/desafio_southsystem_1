Orientações para rodar a aplicação e suas dependências pela linha de comando:

1. Crie o arquivo "productsDB.csv" na pasta "c:\temp".
2. Copie o arquivo disponibilizado "mostruario_fabrica.csv" na pasta "c:\temp"
3. Abra o terminal na pasta principal do projeto.
4. Excute o comando maven "mvn compile".
5. Acesse a pasta interna "desafio_southsystem_1\target\classes" com o terminal.
6. Execute o comando "java application/Program".

obs.: para a correta leitura de caracteres, certifique-se que a fonte do terminal esteja configurada em UTF-8.