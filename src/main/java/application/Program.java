package application;

import java.util.Locale;

import frontend.CenterFacade;
import frontend.UserInterface;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		
		CenterFacade facade = new CenterFacade();
		
		int option = 0;
		System.out.println("Bem-vindo � Loja Online\n\n");
		
		while(option != 5) {
			option = UserInterface.showMainOptions();
			facade.mainMenu(option);
		}

	}

}
