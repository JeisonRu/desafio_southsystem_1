package services;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import domain.entities.Product;
import exceptions.NegativeValueException;

public class ProductServiceTest {
	
	ProductService productService;
	
	@Before
	public void setup() {
		productService = new ProductService();
	}
	
	@Test
	public void mustInstantiateProduct() throws Exception {
		// cen�rio
		Map<String, String> productStr = new HashMap<>();
		productStr.put("name", "C�mera Fotogr�fica");
		productStr.put("price", Double.toString(555.55));
		productStr.put("quantity", Integer.toString(5));
		productStr.put("category", "Eletro");

		// a��o
		Product newProduct = productService.createProduct(productStr);

		// verifica��o
		Assert.assertEquals(productStr.get("name"), newProduct.getName());
		Assert.assertEquals(Double.parseDouble(productStr.get("price")), newProduct.getPrice(), 0.00);
		Assert.assertEquals(Integer.parseInt(productStr.get("quantity")), (int) newProduct.getQuantity());
	}
	
	@Test (expected=NegativeValueException.class) // para tratar de exce��o por pre�o negativo
	public void mustThrowPriceError() throws Exception {
		//cenario
		Map<String, String> productStr = new HashMap<>();
		productStr.put("name", "C�mera Fotogr�fica");
		productStr.put("price", Double.toString(-555.55));
		productStr.put("quantity", Integer.toString(5));
		productStr.put("category", "Eletro");
		
		//acao
		Product newProduct = productService.createProduct(productStr);
	}
	
	@Test (expected=NegativeValueException.class) // para tratar de exce��o por quantidade negativa
	public void mustThrowQuantityError() throws Exception {
		//cenario
		Map<String, String> productStr = new HashMap<>();
		productStr.put("name", "C�mera Fotogr�fica");
		productStr.put("price", Double.toString(555.55));
		productStr.put("quantity", Integer.toString(-5));
		productStr.put("category", "Eletro");
		
		//acao
		Product newProduct = productService.createProduct(productStr);
	}
	
	@Test
	public void mustLoadProducts() {
		// cen�rio
		Locale.setDefault(Locale.US);
		String path = "c:\\temp\\mostruario_fabrica.csv";
		
		// a��o
		List<Product> products = productService.loadNewProducts(path);
		
		//verifica��o
		Assert.assertEquals(58.71d, products.get(3).getPrice(), 0.00);
		Assert.assertEquals("MEIO DE TRANSPORTE", products.get(6).getCategory().getName());
	}
	
}